package com.acuvera.acuveraadmin.http;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//import com.google.android.gms.common.GooglePlayServicesUtil;

public class UserFunctions {
    private static String objective;
    private static String URL = "http://demo.pulsans.com/acuvera/public/api/auth";
    private static String httptype;
    private final JSONParser jsonParser;
    String finalurl;

    public UserFunctions(String objective, String httptype) {
        this.httptype = httptype;
        this.objective = objective;
        jsonParser = new JSONParser();
    }

    public JSONObject GenericFunction(String[] Tags, String[] Values) throws IOException {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        int i = 0;
        for (int j = 0; j < Tags.length; j++) {
            params.add(new BasicNameValuePair(Tags[i], Values[j]));
            i++;
        }
        if (httptype.equals("get")) {
            Log.e("method", httptype);
            String tokenreq = "?key=testandroid&secret=1qazwsx";
            finalurl = URL + "/token" + tokenreq;
        } else if (httptype.equals("post")) {

            finalurl = URL + "/agent";
            Log.e("method", httptype);
        }
        Log.e("Url", "req " + finalurl);
        Log.e("params", "params " + params);

        JSONObject json = jsonParser.getJSONFromUrl(httptype, finalurl, params);
        return json;
    }
}